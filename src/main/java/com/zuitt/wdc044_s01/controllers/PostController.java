package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.services.PostService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
// to indicate that a particular class is responsible for handling incoming HTTP requests and generating appropriate responses.

@CrossOrigin
// used in Java to enable Cross-Origin Resource Sharing (CORS) support in a Spring application. CORS is a security mechanism implemented by web browsers to restrict cross-origin requests made by web applications.
/*
When a client (e.g., a web browser) makes a request to a server, the server typically restricts access to its resources to requests originating from the same origin (i.e., the same domain, protocol, and port). This is known as the Same-Origin Policy enforced by web browsers.

However, there are cases where you might want to allow requests from a different origin to access your server's resources. This is where CORS comes into play. CORS allows controlled access to resources on a different origin.
* */
public class PostController {
    @Autowired
    PostService postService;

    //create a new post
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }


    // Update Post
    @RequestMapping(value="/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value="Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postid, stringToken, post);
    }

    // Delete Post
    @RequestMapping(value="/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value="Authorization") String stringToken){
        return postService.deletePost(postid, stringToken);
    }

    // Get My Posts
    @RequestMapping(value="/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@RequestHeader(value="Authorization") String stringToken) {
        return new ResponseEntity<>(postService.getMyPosts(stringToken), HttpStatus.OK);
    }

}
