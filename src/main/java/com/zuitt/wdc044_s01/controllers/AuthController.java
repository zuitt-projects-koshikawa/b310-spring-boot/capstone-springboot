package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.JwtRequest;
import com.zuitt.wdc044_s01.models.JwtResponse;

import com.zuitt.wdc044_s01.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import com.zuitt.wdc044_s01.config.JwtToken;

@RestController
@CrossOrigin
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtToken jwtToken;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    /* After receiving a /authenticate end-point, and a HTTP method POST, createAuthenticationToken will run*/
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)

    /* @RequestBody, which means it expects the request body of the HTTP request to be mapped to an object of the JwtRequest class. */
    /* Using JwtRequest class, authenticationRequest object is built */
    /* This method can potentially throw an exception if an error occurs during its execution. */
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        /* Uses authenticate function to check if a valid user or not. */
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        /* Saves the user information to userDetails using the class loadUserByUsername of  jwtUserDetailsService to generate a User object using the username as argument */
        final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        /* Then generates a token through the provided userDetails */
        final String token = jwtToken.generateToken(userDetails);

        /* Create a response that includes the generated token. It constructs a response entity with a status code indicating success and wraps the token in a custom object called JwtResponse. This allows the client to receive the token in a specific format, typically as part of a JSON response. The client can then extract and use the token for further authentication or authorization. */
        return ResponseEntity.ok(new JwtResponse(token));

    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
